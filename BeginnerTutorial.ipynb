{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Kryptalgo Beginner Tutorial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Kryptalgo environment is based on the Catalyst framework, which is an open-source algorithmic trading simulator for crypto assets written in Python. The source code can be found [here](https://github.com/enigmampc/catalyst).\n",
    "\n",
    "Some benefits include:\n",
    "\n",
    "* Support for several of the top crypto-exchanges by trading volume.\n",
    "* Realistic: slippage, transaction costs, order delays.\n",
    "* Stream-based: Process each event individually, avoids look-ahead bias.\n",
    "* Batteries included: Common transforms (moving average) as well as common risk calculations (Sharpe).\n",
    "* Developed and continuously updated by Enigma MPC which is building the Enigma data marketplace protocol as well as Catalyst, the first application that will run on our protocol. Powered by our financial data marketplace, Catalyst empowers users to share and curate data and build profitable, data-driven investment strategies.\n",
    "\n",
    "This tutorial assumes that you have Catalyst correctly installed, see the Install section if you haven’t set up Catalyst yet.\n",
    "\n",
    "Every __catalyst__ algorithm consists of at least two functions you have to define:\n",
    "\n",
    "* __initialize(context)__\n",
    "* __handle_data(context, data)__\n",
    "\n",
    "Before the start of the algorithm, __catalyst__ calls the __initialize()__ function and passes in a __context__ variable. __context__ is a persistent namespace for you to store variables you need to access from one algorithm iteration to the next.\n",
    "\n",
    "After the algorithm has been initialized, __catalyst__ calls the __handle_data()__ function on each iteration, that’s one per day (daily) or once every minute (minute), depending on the frequency we choose to run our simulation. On every iteration, __handle_data()__ passes the same __context__ variable and an event-frame called __data__ containing the current trading bar with open, high, low, and close (OHLC) prices as well as volume for each crypto asset in your universe."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## My first algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets take a look at a very simple algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "from catalyst.api import order, record, symbol\n",
    "\n",
    "\n",
    "def initialize(context):\n",
    "    context.asset = symbol('btc_usd')\n",
    "\n",
    "\n",
    "def handle_data(context, data):\n",
    "    order(context.asset, 1)\n",
    "    record(btc = data.current(context.asset, 'price'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, we first have to import some functions we would like to use. All functions commonly used in your algorithm can be found in __catalyst.api__. Here we are using __order()__ which takes two arguments: a cryptoasset object, and a number specifying how many assets you would like to order (if negative, __order()__ will sell assets). In this case we want to order 1 bitcoin at each iteration.\n",
    "\n",
    "Finally, the __record()__ function allows you to save the value of a variable at each iteration. You provide it with a name for the variable together with the variable itself: __varname=var__. After the algorithm finished running you will have access to each variable value you tracked with __record()__ under the name you provided (we will see this further below). You also see how we can access the current price data of a bitcoin in the __data__ event frame"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ingesting data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before you can backtest your algorithm, you first need to load the historical pricing data that Catalyst needs to run your simulation through a process called ingestion. When you ingest data, Catalyst downloads that data in compressed form from the Enigma servers (which eventually will migrate to the Enigma Data Marketplace), and stores it locally to make it available at runtime.\n",
    "\n",
    "\n",
    "$\\color{red}{\\text{In order to ingest data, you need to run a command like the following:}}$\n"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "catalyst ingest-exchange -x bitfinex -i btc_usd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This instructs Catalyst to download pricing data from the Bitfinex exchange for the btc_usd currency pair (this follows from the simple algorithm presented above where we want to trade btc_usd), and we’re choosing to test our algorithm using historical pricing data from the Bitfinex exchange. By default, Catalyst assumes that you want data with daily frequency (one candle bar per day). If you want instead minute frequency (one candle bar for every minute), you would need to specify it as follows:\n"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "catalyst ingest-exchange -x bitfinex -i btc_usd -f minute\n",
    "Ingesting exchange bundle bitfinex...\n",
    "  [====================================]  Ingesting daily price data on bitfinex:  100%"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We believe it is important for you to have a high-level understanding of how data is managed, hence the following overview:\n",
    "\n",
    "* Pricing data is split and packaged into __bundles__: chunks of data organized as time series that are kept up to date daily on Enigma’s servers. Catalyst downloads the requested bundles and reconstructs the full dataset in your hard drive.\n",
    "* Pricing data is provided in __daily__ and __minute__ resolution. Those are different bundle datasets, and are managed separately.\n",
    "* Bundles are exchange-specific, as the pricing data is specific to the trades that happen in each exchange. As a result, you must specify which exchange you want pricing data from when ingesting data.\n",
    "* Catalyst keeps track of all the downloaded bundles, so that it only has to download them once, and will do incremental updates as needed.\n",
    "* When running in live __trading mode__, Catalyst will first look for historical pricing data in the locally stored bundles. If there is anything missing, Catalyst will hit the exchange for the most recent data, and merge it with the local bundle to optimize the number of requests it needs to make to the exchange.\n",
    "\n",
    "The __ingest-exchange__ command in catalyst offers additional parameters to further tweak the data ingestion process. You can learn more by running the following from the command line:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "catalyst ingest-exchange --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running the algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now test your algorithm using cryptoassets’ historical pricing data, catalyst provides three interfaces:\n",
    "\n",
    "* A command-line interface (__CLI__),\n",
    "* a __run_algorithm()__ that you can call from other Python scripts,\n",
    "* and the __Jupyter Notebook magic__.\n",
    "\n",
    "In this Notebook we will cover the __CLI__, and introduce the __run_algorithm()__ in the last example of this tutorial. Some of the example algorithms provide instructions on how to run them both from the __CLI__, and using the __run_algorithm()__ function. We will mostly use the third method and different examples of trading strategies using __Jupyter Notebook magic__ have been prepared."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Command line interface"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should be able to execute the following from your command line:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "$ catalyst --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the resulting output:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Usage: catalyst [OPTIONS] COMMAND [ARGS]...\n",
    "\n",
    "  Top level catalyst entry point.\n",
    "\n",
    "Options:\n",
    "  -e, --extension TEXT            File or module path to a catalyst extension\n",
    "                                  to load.\n",
    "  --strict-extensions / --non-strict-extensions\n",
    "                                  If --strict-extensions is passed then\n",
    "                                  catalyst will not run if it cannot load all\n",
    "                                  of the specified extensions. If this is not\n",
    "                                  passed or --non-strict-extensions is passed\n",
    "                                  then the failure will be logged but\n",
    "                                  execution will continue.\n",
    "  --default-extension / --no-default-extension\n",
    "                                  Don't load the default catalyst extension.py\n",
    "                                  file in $CATALYST_HOME.\n",
    "  --version                       Show the version and exit.\n",
    "  --help                          Show this message and exit.\n",
    "\n",
    "Commands:\n",
    "  bundles          List all of the available data bundles.\n",
    "  clean            Clean up bundles from 'ingest'.\n",
    "  clean-algo\n",
    "  clean-exchange   Clean up bundles from 'ingest-exchange'.\n",
    "  ingest           Ingest the data for the given bundle.\n",
    "  ingest-exchange  Ingest data for the given exchange.\n",
    "  live             Trade live with the given algorithm.\n",
    "  marketplace      Access the Enigma Data Marketplace to: -...\n",
    "  remote-run       Run a backtest for the given algorithm on the...\n",
    "  remote-status    Get the status of a running algorithm on the...\n",
    "  run              Run a backtest for the given algorithm."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are three main modes you can run on Catalyst. The first being __ingest-exchange__ for data ingestion, which we have covered in the previous section. The second is __live__ to use your algorithm to trade live against a given exchange, and the third mode run is to __backtest__ your algorithm before trading live with it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let’s start with backtesting, so run this other command to learn more about the available options:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "catalyst run --help"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Usage: catalyst run [OPTIONS]\n",
    "\n",
    "  Run a backtest for the given algorithm.\n",
    "\n",
    "Options:\n",
    "  -f, --algofile FILENAME         The file that contains the algorithm to run.\n",
    "  -t, --algotext TEXT             The algorithm script to run.\n",
    "  -D, --define TEXT               Define a name to be bound in the namespace\n",
    "                                  before executing the algotext. For example\n",
    "                                  '-Dname=value'. The value may be any python\n",
    "                                  expression. These are evaluated in order so\n",
    "                                  they may refer to previously defined names.\n",
    "  --data-frequency [daily|minute]\n",
    "                                  The data frequency of the simulation.\n",
    "                                  [default: daily]\n",
    "  --capital-base FLOAT            The starting capital for the simulation.\n",
    "  -b, --bundle BUNDLE-NAME        The data bundle to use for the simulation.\n",
    "                                  [default: poloniex]\n",
    "  --bundle-timestamp TIMESTAMP    The date to lookup data on or before.\n",
    "                                  [default: <current-time>]\n",
    "  -s, --start DATE                The start date of the simulation.\n",
    "  -e, --end DATE                  The end date of the simulation.\n",
    "  -o, --output FILENAME           The location to write the perf data. If this\n",
    "                                  is '-' the perf will be written to stdout.\n",
    "                                  [default: -]\n",
    "  --print-algo / --no-print-algo  Print the algorithm to stdout.\n",
    "  -x, --exchange-name TEXT        The name of the targeted exchange.\n",
    "  -n, --algo-namespace TEXT       A label assigned to the algorithm for data\n",
    "                                  storage purposes.\n",
    "  -c, --quote-currency TEXT       The quote currency used to calculate\n",
    "                                  statistics (e.g. usd, btc, eth).\n",
    "  --help                          Show this message and exit."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see there are a couple of flags that specify where to find your algorithm (__-f__) as well as a the __-x__ flag to specify which exchange to use. There are also arguments for the date range to run the algorithm over (__--start__ and __--end__). You also need to set the quote currency for your algorithm through the __-c__ flag, and the __--capital_base__. All the aforementioned parameters are required. Optionally, you will want to save the performance metrics of your algorithm so that you can analyze how it performed. This is done via the __--output__ flag and will cause it to write the performance __DataFrame__ in the pickle Python file format. Note that you can also define a configuration file with these parameters that you can then conveniently pass to the __-c__ option so that you don’t have to supply the command line args all the time.\n",
    "\n",
    "Thus, to execute our algorithm from above and save the results to __buy_btc_simple_out.pickle__ we would call __catalyst run__ as follows:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "catalyst run -f buy_btc_simple.py -x bitfinex --start 2016-1-1 --end 2017-9-30 -c usd --capital-base 100000 -o buy_btc_simple_out.pickle"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "INFO: run_algo: running algo in backtest mode\n",
    "INFO: exchange_algorithm: initialized trading algorithm in backtest mode\n",
    "INFO: Performance: Simulated 639 trading days out of 639.\n",
    "INFO: Performance: first open: 2016-01-01 00:00:00+00:00\n",
    "INFO: Performance: last close: 2017-09-30 23:59:00+00:00"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__run__ first calls the __initialize()__ function, and then streams the historical asset price day-by-day through __handle_data()__. After each call to __handle_data()__ we instruct __catalyst__ to order 1 bitcoin. After the call of the __order()__ function, __catalyst__ enters the ordered stock and amount in the order book. After the __handle_data()__ function has finished, catalyst looks for any open orders and tries to fill them. If the trading volume is high enough for this asset, the order is executed after adding the commission and applying the slippage model which models the influence of your order on the stock price, so your algorithm will be charged more than just the asset price. (Note, that you can also change the commission and slippage model that __catalyst__ uses).\n",
    "\n",
    "Let’s take a quick look at the performance __DataFrame__. For this, we write different Python script–let’s call it __print_results.py__ and we make use of the fantastic __pandas__ library to print the first ten rows. Note that __catalyst__ makes heavy usage of [pandas](http://pandas.pydata.org/), especially for data analysis and outputting so it’s worth spending some time to learn it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>algo_volatility</th>\n",
       "      <th>algorithm_period_return</th>\n",
       "      <th>alpha</th>\n",
       "      <th>benchmark_period_return</th>\n",
       "      <th>benchmark_volatility</th>\n",
       "      <th>beta</th>\n",
       "      <th>btc</th>\n",
       "      <th>capital_used</th>\n",
       "      <th>ending_cash</th>\n",
       "      <th>ending_exposure</th>\n",
       "      <th>...</th>\n",
       "      <th>short_exposure</th>\n",
       "      <th>short_value</th>\n",
       "      <th>shorts_count</th>\n",
       "      <th>sortino</th>\n",
       "      <th>starting_cash</th>\n",
       "      <th>starting_exposure</th>\n",
       "      <th>starting_value</th>\n",
       "      <th>trading_days</th>\n",
       "      <th>transactions</th>\n",
       "      <th>treasury_period_return</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>2016-01-01 23:59:00+00:00</th>\n",
       "      <td>NaN</td>\n",
       "      <td>0.000000</td>\n",
       "      <td>None</td>\n",
       "      <td>0.001000</td>\n",
       "      <td>NaN</td>\n",
       "      <td>None</td>\n",
       "      <td>433.98</td>\n",
       "      <td>0.000000</td>\n",
       "      <td>100000.000000</td>\n",
       "      <td>0.00</td>\n",
       "      <td>...</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>100000.000000</td>\n",
       "      <td>0.00</td>\n",
       "      <td>0.00</td>\n",
       "      <td>1</td>\n",
       "      <td>[]</td>\n",
       "      <td>0.001</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2016-01-02 23:59:00+00:00</th>\n",
       "      <td>0.000102</td>\n",
       "      <td>-0.000009</td>\n",
       "      <td>None</td>\n",
       "      <td>0.002001</td>\n",
       "      <td>0.0</td>\n",
       "      <td>None</td>\n",
       "      <td>432.70</td>\n",
       "      <td>-433.608757</td>\n",
       "      <td>99566.391243</td>\n",
       "      <td>432.70</td>\n",
       "      <td>...</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>-11.224972</td>\n",
       "      <td>100000.000000</td>\n",
       "      <td>0.00</td>\n",
       "      <td>0.00</td>\n",
       "      <td>2</td>\n",
       "      <td>[{'amount': 1, 'dt': 2016-01-02 23:59:00+00:00...</td>\n",
       "      <td>0.001</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2016-01-03 23:59:00+00:00</th>\n",
       "      <td>0.000442</td>\n",
       "      <td>-0.000061</td>\n",
       "      <td>None</td>\n",
       "      <td>0.003003</td>\n",
       "      <td>0.0</td>\n",
       "      <td>None</td>\n",
       "      <td>428.39</td>\n",
       "      <td>-429.289705</td>\n",
       "      <td>99137.101539</td>\n",
       "      <td>856.78</td>\n",
       "      <td>...</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>-10.603751</td>\n",
       "      <td>99566.391243</td>\n",
       "      <td>432.70</td>\n",
       "      <td>432.70</td>\n",
       "      <td>3</td>\n",
       "      <td>[{'amount': 1, 'dt': 2016-01-03 23:59:00+00:00...</td>\n",
       "      <td>0.001</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2016-01-04 23:59:00+00:00</th>\n",
       "      <td>0.000883</td>\n",
       "      <td>0.000020</td>\n",
       "      <td>None</td>\n",
       "      <td>0.004006</td>\n",
       "      <td>0.0</td>\n",
       "      <td>None</td>\n",
       "      <td>432.90</td>\n",
       "      <td>-433.809177</td>\n",
       "      <td>98703.292362</td>\n",
       "      <td>1298.70</td>\n",
       "      <td>...</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>2.990961</td>\n",
       "      <td>99137.101539</td>\n",
       "      <td>856.78</td>\n",
       "      <td>856.78</td>\n",
       "      <td>4</td>\n",
       "      <td>[{'amount': 1, 'dt': 2016-01-04 23:59:00+00:00...</td>\n",
       "      <td>0.001</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2016-01-05 23:59:00+00:00</th>\n",
       "      <td>0.000831</td>\n",
       "      <td>-0.000021</td>\n",
       "      <td>None</td>\n",
       "      <td>0.005010</td>\n",
       "      <td>0.0</td>\n",
       "      <td>None</td>\n",
       "      <td>431.84</td>\n",
       "      <td>-432.746950</td>\n",
       "      <td>98270.545412</td>\n",
       "      <td>1727.36</td>\n",
       "      <td>...</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>-2.224317</td>\n",
       "      <td>98703.292362</td>\n",
       "      <td>1298.70</td>\n",
       "      <td>1298.70</td>\n",
       "      <td>5</td>\n",
       "      <td>[{'amount': 1, 'dt': 2016-01-05 23:59:00+00:00...</td>\n",
       "      <td>0.001</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>5 rows × 39 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "                           algo_volatility  algorithm_period_return alpha  \\\n",
       "2016-01-01 23:59:00+00:00              NaN                 0.000000  None   \n",
       "2016-01-02 23:59:00+00:00         0.000102                -0.000009  None   \n",
       "2016-01-03 23:59:00+00:00         0.000442                -0.000061  None   \n",
       "2016-01-04 23:59:00+00:00         0.000883                 0.000020  None   \n",
       "2016-01-05 23:59:00+00:00         0.000831                -0.000021  None   \n",
       "\n",
       "                           benchmark_period_return  benchmark_volatility  \\\n",
       "2016-01-01 23:59:00+00:00                 0.001000                   NaN   \n",
       "2016-01-02 23:59:00+00:00                 0.002001                   0.0   \n",
       "2016-01-03 23:59:00+00:00                 0.003003                   0.0   \n",
       "2016-01-04 23:59:00+00:00                 0.004006                   0.0   \n",
       "2016-01-05 23:59:00+00:00                 0.005010                   0.0   \n",
       "\n",
       "                           beta     btc  capital_used    ending_cash  \\\n",
       "2016-01-01 23:59:00+00:00  None  433.98      0.000000  100000.000000   \n",
       "2016-01-02 23:59:00+00:00  None  432.70   -433.608757   99566.391243   \n",
       "2016-01-03 23:59:00+00:00  None  428.39   -429.289705   99137.101539   \n",
       "2016-01-04 23:59:00+00:00  None  432.90   -433.809177   98703.292362   \n",
       "2016-01-05 23:59:00+00:00  None  431.84   -432.746950   98270.545412   \n",
       "\n",
       "                           ending_exposure           ...            \\\n",
       "2016-01-01 23:59:00+00:00             0.00           ...             \n",
       "2016-01-02 23:59:00+00:00           432.70           ...             \n",
       "2016-01-03 23:59:00+00:00           856.78           ...             \n",
       "2016-01-04 23:59:00+00:00          1298.70           ...             \n",
       "2016-01-05 23:59:00+00:00          1727.36           ...             \n",
       "\n",
       "                           short_exposure  short_value  shorts_count  \\\n",
       "2016-01-01 23:59:00+00:00               0            0             0   \n",
       "2016-01-02 23:59:00+00:00               0            0             0   \n",
       "2016-01-03 23:59:00+00:00               0            0             0   \n",
       "2016-01-04 23:59:00+00:00               0            0             0   \n",
       "2016-01-05 23:59:00+00:00               0            0             0   \n",
       "\n",
       "                             sortino  starting_cash  starting_exposure  \\\n",
       "2016-01-01 23:59:00+00:00        NaN  100000.000000               0.00   \n",
       "2016-01-02 23:59:00+00:00 -11.224972  100000.000000               0.00   \n",
       "2016-01-03 23:59:00+00:00 -10.603751   99566.391243             432.70   \n",
       "2016-01-04 23:59:00+00:00   2.990961   99137.101539             856.78   \n",
       "2016-01-05 23:59:00+00:00  -2.224317   98703.292362            1298.70   \n",
       "\n",
       "                           starting_value  trading_days  \\\n",
       "2016-01-01 23:59:00+00:00            0.00             1   \n",
       "2016-01-02 23:59:00+00:00            0.00             2   \n",
       "2016-01-03 23:59:00+00:00          432.70             3   \n",
       "2016-01-04 23:59:00+00:00          856.78             4   \n",
       "2016-01-05 23:59:00+00:00         1298.70             5   \n",
       "\n",
       "                                                                transactions  \\\n",
       "2016-01-01 23:59:00+00:00                                                 []   \n",
       "2016-01-02 23:59:00+00:00  [{'amount': 1, 'dt': 2016-01-02 23:59:00+00:00...   \n",
       "2016-01-03 23:59:00+00:00  [{'amount': 1, 'dt': 2016-01-03 23:59:00+00:00...   \n",
       "2016-01-04 23:59:00+00:00  [{'amount': 1, 'dt': 2016-01-04 23:59:00+00:00...   \n",
       "2016-01-05 23:59:00+00:00  [{'amount': 1, 'dt': 2016-01-05 23:59:00+00:00...   \n",
       "\n",
       "                           treasury_period_return  \n",
       "2016-01-01 23:59:00+00:00                   0.001  \n",
       "2016-01-02 23:59:00+00:00                   0.001  \n",
       "2016-01-03 23:59:00+00:00                   0.001  \n",
       "2016-01-04 23:59:00+00:00                   0.001  \n",
       "2016-01-05 23:59:00+00:00                   0.001  \n",
       "\n",
       "[5 rows x 39 columns]"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import pandas as pd\n",
    "perf = pd.read_pickle('buy_btc_simple_out.pickle') # read in perf DataFrame\n",
    "perf.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a row for each trading day, starting on the first day of our simulation Jan 1st, 2016. In the columns you can find various information about the state of your algorithm. The column __btc__ was placed there by the __record()__ function mentioned earlier and allows us to plot the price of bitcoin. For example, we could easily examine now how our portfolio value changed over time compared to the bitcoin price.\n",
    "\n",
    "Now we will run the simulation again, but this time we extend our original algorithm with the addition of the __analyze()__ function. Somewhat analogously as how __initialize()__ gets called once before the start of the algorithm, __analyze()__ gets called once at the end of the algorithm, and receives two variables: __context__, which we discussed at the very beginning, and perf, which is the pandas dataframe containing the performance data for our algorithm that we reviewed above. Inside the __analyze()__ function is where we can analyze and visualize the results of our strategy. Here’s the revised simple algorithm (note the addition of Line 1, and Lines 11-18)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from catalyst.api import order, record, symbol\n",
    "\n",
    "def initialize(context):\n",
    "    context.asset = symbol('btc_usd')\n",
    "\n",
    "def handle_data(context, data):\n",
    "    order(context.asset, 1)\n",
    "    record(btc = data.current(context.asset, 'price'))\n",
    "\n",
    "def analyze(context, perf):\n",
    "    ax1 = plt.subplot(211)\n",
    "    perf.portfolio_value.plot(ax=ax1)\n",
    "    ax1.set_ylabel('portfolio value')\n",
    "    ax2 = plt.subplot(212, sharex=ax1)\n",
    "    perf.btc.plot(ax=ax2)\n",
    "    ax2.set_ylabel('bitcoin price')\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we make use of the external visualization library called [matplotlib](https://matplotlib.org/), which you might recall we installed alongside enigma-catalyst (with the exception of the __Conda__ install, where it was included by default inside the conda environment we created). If for any reason you don’t have it installed, you can add it by running:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "(catalyst)$ pip install matplotlib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Jupyter Notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Jupyter Notebook is a very powerful browser-based interface to a Python interpreter. As it is already the de-facto interface for most quantitative researchers, catalyst provides an easy way to run your algorithm inside the Notebook without requiring you to use the CLI.\n",
    "\n",
    "To use it you have to write your algorithm in a cell and let catalyst know that it is supposed to run this algorithm. This is done via the __*%%catalyst*__ IPython magic command that is available after you import catalyst from within the Notebook. This magic takes the same arguments as the command line interface. Thus, to run the algorithm just supply the same parameters as the CLI but without the -f and -o arguments. We just have to execute the following cell after importing catalyst to register the magic.\n",
    "\n",
    "Please remember: \n",
    "- $\\color{red}{\\text{ingest the data by running an appropriate command from Terminal}}$ since that functionality is not supported from within the Notebook  \n",
    "- register the catalyst magic:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "%load_ext catalyst"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
